-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2018 at 01:49 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lakin`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_lakin`
--

CREATE TABLE `tb_lakin` (
  `id_lakin` int(6) NOT NULL,
  `tgl_lakin` date NOT NULL,
  `uraian_lakin` varchar(255) NOT NULL,
  `mulai_lakin` time NOT NULL,
  `selesai_lakin` time NOT NULL,
  `lama_lakin` varchar(50) NOT NULL,
  `output_lakin` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_lakin`
--

INSERT INTO `tb_lakin` (`id_lakin`, `tgl_lakin`, `uraian_lakin`, `mulai_lakin`, `selesai_lakin`, `lama_lakin`, `output_lakin`) VALUES
(1, '2018-09-28', 'Upacara pelepasan Siswa PKL', '08:00:00', '10:30:00', '150 Menit', '1 Kegiatan'),
(2, '2018-09-28', 'Pengarahan siswa PKL Kelas XII Multimedia', '11:00:00', '12:00:00', '60 Menit', '1 Kegiatan'),
(3, '2018-09-27', 'Mengajar', '07:30:00', '09:30:00', '120 Menit', '1 Kegiatan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_lakin`
--
ALTER TABLE `tb_lakin`
  ADD PRIMARY KEY (`id_lakin`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_lakin`
--
ALTER TABLE `tb_lakin`
  MODIFY `id_lakin` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
