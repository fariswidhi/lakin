<?php 
ini_set('display_errors', 1);
error_reporting(E_ALL);

include "models/m_lakin.php";


$lakin =  new Lakin("localhost","root","root","lakin");

$userid = $_SESSION['userid'];
$sql = "SELECT * FROM tb_user where id_user='$userid'";
$query = mysqli_query($conn,$sql);

$data = mysqli_fetch_array($query);

$level = $data['level'];

if ($level=="1") {
if(@$_GET['act'] == '') {
?>

		<div class="row">
          <div class="col-lg-12">
            <h1>Data User</h1>

          </div>
        </div>

        <div class="row">
        	<div class="col-lg-12">
        		
        		

				<div class="table-responsive">
					
					<?php


$user = $_SESSION['userid'];
$sql = "SELECT * FROM tb_lakin where id_user='$user'";
$query = mysqli_query($conn,$sql) or die(mysqli_error($conn));
?>
<?php
$sql = 'SELECT * FROM tb_user';
$query = mysqli_query($conn,$sql) or die(mysqli_error($conn));
?>

<table class="table table-striped" id="datatables">
<thead>
			
<th>Username</th>
				
				
<th>Level</th>
				
<th>Nama lengkap</th>
		<th>Aksi</th>
</thead>
	
<tbody>
<?php while($d=mysqli_fetch_object($query)): ?>

<tr>
						
								

	<td><?php  echo $d->username; ?></td>
								

	<td><?php  echo $d->level=="2"?"Guru":"Kepala Sekolah"; ?></td>
								

	<td><?php  echo $d->nama_lengkap; ?></td>
						<td>
		<a class="btn btn-success" href="?page=user&act=edit&act=edit&id=<?php echo $d->id_user; ?>">Ubah</a>
	<a class="btn btn-danger" href="?page=user&act=del&id=<?php echo $d->id_user; ?>">Hapus</a>

	</td>
</tr>
<?php endwhile ?>
</tbody>
</table>






				</div>
<!-- 
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#tambah">Tambah Data</button> -->
				<a href="index.php?page=user&act=tambah" class="btn btn-success">Tambah Data</a>

				<div id="tambah" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Tambah Laporan</h4>
							</div>
							<form action="" method="post" enctype="multipart/form-data">
								<div class="modal-body">
									<div class="form-group">
										<label class="control-label" for="tgl_lakin">Tanggal</label>
										<input type="date" name="tgl_lakin" class="form-control" id="tgl_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="uraian_lakin">Kegiatan</label>
										<input type="text" name="uraian_lakin" class="form-control" id="uraian_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="mulai_lakin">Mulai Pengerjaan</label>
										<input type="time" name="mulai_lakin" class="form-control" id="mulai_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="selesai_lakin">Selesai Pengerjaan</label>
										<input type="time" name="selesai_lakin" class="form-control" id="selesai_lakin" required>
									</div>
<!-- 									<div class="form-group">
										<label class="control-label" for="lama_lakin">Lama Pengerjaan (Menit)</label>
										<input type="text" name="lama_lakin" class="form-control" id="lama_lakin" required>
									</div> -->
									<div class="form-group">
										<label class="control-label" for="output_lakin">Kuantitas / Output</label>
										<input type="text" name="output_lakin" class="form-control" id="output_lakin" required>
									</div>
								</div>
								<div class="modal-footer">
									<button type="reset" class="btn btn-danger">Reset</button>
									<input type="submit" class="btn btn-success" name="tambah" value="Simpan">
								</div>
							</form>
							<?php 
							if (@$_POST['tambah']) {
								$tgl_lakin = $connection->conn->real_escape_string($_POST['tgl_lakin']);
								$uraian_lakin = $connection->conn->real_escape_string($_POST['uraian_lakin']);
								$mulai_lakin = $connection->conn->real_escape_string($_POST['mulai_lakin']);
								$selesai_lakin = $connection->conn->real_escape_string($_POST['selesai_lakin']);
								// $lama_lakin = $connection->conn->real_escape_string($_POST['lama_lakin']);
								$output_lakin = $connection->conn->real_escape_string($_POST['output_lakin']);
												
								$lakin->tambah($tgl_lakin, $uraian_lakin, $mulai_lakin, $selesai_lakin, 0, $output_lakin);
								header("location: ?page=lakin");
								
							}
							?>
						</div>
					</div>
				</div>



				<div id="edit" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Edit Laporan</h4>
							</div>
							<form id="form" enctype="multipart/form-data">
								<div class="modal-body" id="modal-edit">
									<div class="form-group">
										<label class="control-label" for="tgl_lakin">Tanggal</label>
										<input type="hidden" name="id_lakin" id="id_lakin">
										<input type="date" name="tgl_lakin" class="form-control" id="tgl_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="uraian_lakin">Kegiatan</label>
										<input type="text" name="uraian_lakin" class="form-control" id="uraian_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="mulai_lakin">Mulai Pengerjaan</label>
										<input type="time" name="mulai_lakin" class="form-control" id="mulai_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="selesai_lakin">Selesai Pengerjaan</label>
										<input type="time" name="selesai_lakin" class="form-control" id="selesai_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="lama_lakin">Lama Pengerjaan (Menit)</label>
										<input type="text" name="lama_lakin" class="form-control" id="lama_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="output_lakin">Kuantitas / Output</label>
										<input type="text" name="output_lakin" class="form-control" id="output_lakin" required>
									</div>
								</div>
								<div class="modal-footer">
									<input type="submit" class="btn btn-success" name="edit" value="Simpan">
								</div>
							</form>
						</div>
					</div>
				</div>


				<script src="assets/js/jquery-1.10.2.js"></script>
				<script type="text/javascript">
					$(document).on("click", "#edit_lakin", function(){
						var idlakin = $(this).data('id');
						var tgllakin = $(this).data('tgl');
						var uraianlakin = $(this).data('uraian');
						var mulailakin = $(this).data('mulai');
						var selesailakin = $(this).data('selesai');
						var lamalakin = $(this).data('lama');
						var outputlakin = $(this).data('output');
						$(".modal-body #id_lakin").val(idlakin);
						$(".modal-body #tgl_lakin").val(tgllakin);
						$(".modal-body #uraian_lakin").val(uraianlakin);
						$(".modal-body #mulai_lakin").val(mulailakin);
						$(".modal-body #selesai_lakin").val(selesailakin);
						$(".modal-body #lama_lakin").val(lamalakin);
						$(".modal-body #output_lakin").val(outputlakin);
					})


					$(document).ready(function(e){
						$("#form").on("submit", (function(e){
							e.preventDefault();
							$.ajax({
								url : 'models/proses_edit_lakin.php',
								type : 'POST',
								data : new FormData(this),
								contentType : false,
								cache : false,
								processData : false,
								success : function(msg) {
									$('.table').html(msg);
								}
							})
						}))
					})
				</script>

        	</div>
        	
        </div>



<?php 
} elseif(@$_GET['act'] == 'del') {
	// $lakin->hapus($_GET['id']);
	// header("location: ?page=lakin");

	$id = $_GET['id'];

	$sql = "DELETE FROM tb_lakin WHERE id_lakin='$id'";
	$query = mysqli_query($conn,$sql) or die(mysqli_error($conn));
	if ($query) {
		# code...
		header("location: ".$_SERVER['HTTP_REFERER']);
	}
}

elseif (@$_GET['act']=='edit') {
	# code...e
	$id = @$_GET['id'];

	include('user/edit.php');
// echo $id;
}
elseif (@$_GET['act']=='tambah') {

	include('user/tambah.php');

}




}
else{
?>
<h1>Anda Tidak Mempunyai Akses Ke Halaman Ini</h1>
<?php
}
?>