<?php 
ini_set('display_errors', 1);
error_reporting(E_ALL);

include "models/m_lakin.php";


$lakin =  new Lakin("localhost","root","root","lakin");

if(@$_GET['act'] == '') {
?>

		<div class="row">
          <div class="col-lg-12">
            <h1>L-Kin <small>Laporan Kinerja</small></h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="icon-dashboard"></i> L-Kin</a></li>
            </ol>
          </div>
        </div>

        <div class="row">
        	<div class="col-lg-12">
        		
        		
        		<a target="_blanLI" href="pdf/laporan.php" class="btn btn-primary">Cetak PDF</a><br>
				<div class="table-responsive">
					<table class="table table-bordered table-hover table-striped" id="datatables">
						<thead>
							<tr>
								<th>No.</th>
								<th>Tanggal</th>
								<th>Kegiatan</th>
								<th>Mulai Pengerjaan</th>
								<th>Selesai Pengerjaan</th>
								<th>Lama Pengerjaan</th>
								<th>Kuantitas / Output</th>
								<th>Opsi</th>
							</tr>
						</thead>
						<tbody>
						<?php 

						$no = 1;
						// $tampil = $lakin->tampil();

						$sql = "SELECT *, (TIME_TO_SEC(selesai_lakin) - TIME_TO_SEC(mulai_lakin))/60 AS `menit` FROM tb_lakin ORDER BY tgl_lakin DESC ";
						$query  = mysqli_query($conn,$sql);

						while($data = mysqli_fetch_object($query)) {
						?>
						<tr>
							<td align="center"><?php echo $no++."."; ?></td>
							<td><?php echo $data->tgl_lakin; ?></td>
							<td><?php echo $data->uraian_lakin; ?></td>
							<td><?php echo $data->mulai_lakin; ?></td>
							<td><?php echo $data->selesai_lakin; ?></td>
							<td><?php echo (int) $data->menit; ?></td>
							<td><?php echo $data->output_lakin; ?></td>
							<td align="center">
								<a id="edit_lakin" data-toggle="modal" data-target="#edit" data-id="<?php echo $data->id_lakin; ?>" data-tgl="<?php echo $data->tgl_lakin; ?>" data-uraian="<?php echo $data->uraian_lakin; ?>" data-mulai="<?php echo $data->mulai_lakin; ?>" data-selesai="<?php echo $data->selesai_lakin; ?>" data-lama="<?php echo $data->lama_lakin; ?>" data-output="<?php echo $data->output_lakin; ?>">
									<button class="btn btn-info btn-xs" title="edit"><i class="fa fa-edit"></i></button>  
								</a>
								<a href="?page=lakin&act=del&id=<?php echo $data->id_lakin; ?>" onclick="return confirm('Yakin akan menghapus laporan ini?')">
								<button class="btn btn-danger btn-xs" title="hapus"><i class="fa fa-trash-o"></i></button>
								</a>
							</td>
						</tr>
						<?php 
						} ?>
						</tbody>
					</table>
					
				</div>

				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#tambah">Tambah Data</button>

				<div id="tambah" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Tambah Laporan</h4>
							</div>
							<form action="" method="post" enctype="multipart/form-data">
								<div class="modal-body">
									<div class="form-group">
										<label class="control-label" for="tgl_lakin">Tanggal</label>
										<input type="date" name="tgl_lakin" class="form-control" id="tgl_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="uraian_lakin">Kegiatan</label>
										<input type="text" name="uraian_lakin" class="form-control" id="uraian_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="mulai_lakin">Mulai Pengerjaan</label>
										<input type="time" name="mulai_lakin" class="form-control" id="mulai_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="selesai_lakin">Selesai Pengerjaan</label>
										<input type="time" name="selesai_lakin" class="form-control" id="selesai_lakin" required>
									</div>
<!-- 									<div class="form-group">
										<label class="control-label" for="lama_lakin">Lama Pengerjaan (Menit)</label>
										<input type="text" name="lama_lakin" class="form-control" id="lama_lakin" required>
									</div> -->
									<div class="form-group">
										<label class="control-label" for="output_lakin">Kuantitas / Output</label>
										<input type="text" name="output_lakin" class="form-control" id="output_lakin" required>
									</div>
								</div>
								<div class="modal-footer">
									<button type="reset" class="btn btn-danger">Reset</button>
									<input type="submit" class="btn btn-success" name="tambah" value="Simpan">
								</div>
							</form>
							<?php 
							if (@$_POST['tambah']) {
								$tgl_lakin = $connection->conn->real_escape_string($_POST['tgl_lakin']);
								$uraian_lakin = $connection->conn->real_escape_string($_POST['uraian_lakin']);
								$mulai_lakin = $connection->conn->real_escape_string($_POST['mulai_lakin']);
								$selesai_lakin = $connection->conn->real_escape_string($_POST['selesai_lakin']);
								// $lama_lakin = $connection->conn->real_escape_string($_POST['lama_lakin']);
								$output_lakin = $connection->conn->real_escape_string($_POST['output_lakin']);
												
								$lakin->tambah($tgl_lakin, $uraian_lakin, $mulai_lakin, $selesai_lakin, 0, $output_lakin);
								header("location: ?page=lakin");
								
							}
							?>
						</div>
					</div>
				</div>



				<div id="edit" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Edit Laporan</h4>
							</div>
							<form id="form" enctype="multipart/form-data">
								<div class="modal-body" id="modal-edit">
									<div class="form-group">
										<label class="control-label" for="tgl_lakin">Tanggal</label>
										<input type="hidden" name="id_lakin" id="id_lakin">
										<input type="date" name="tgl_lakin" class="form-control" id="tgl_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="uraian_lakin">Kegiatan</label>
										<input type="text" name="uraian_lakin" class="form-control" id="uraian_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="mulai_lakin">Mulai Pengerjaan</label>
										<input type="time" name="mulai_lakin" class="form-control" id="mulai_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="selesai_lakin">Selesai Pengerjaan</label>
										<input type="time" name="selesai_lakin" class="form-control" id="selesai_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="lama_lakin">Lama Pengerjaan (Menit)</label>
										<input type="text" name="lama_lakin" class="form-control" id="lama_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="output_lakin">Kuantitas / Output</label>
										<input type="text" name="output_lakin" class="form-control" id="output_lakin" required>
									</div>
								</div>
								<div class="modal-footer">
									<input type="submit" class="btn btn-success" name="edit" value="Simpan">
								</div>
							</form>
						</div>
					</div>
				</div>


				<script src="assets/js/jquery-1.10.2.js"></script>
				<script type="text/javascript">
					$(document).on("click", "#edit_lakin", function(){
						var idlakin = $(this).data('id');
						var tgllakin = $(this).data('tgl');
						var uraianlakin = $(this).data('uraian');
						var mulailakin = $(this).data('mulai');
						var selesailakin = $(this).data('selesai');
						var lamalakin = $(this).data('lama');
						var outputlakin = $(this).data('output');
						$(".modal-body #id_lakin").val(idlakin);
						$(".modal-body #tgl_lakin").val(tgllakin);
						$(".modal-body #uraian_lakin").val(uraianlakin);
						$(".modal-body #mulai_lakin").val(mulailakin);
						$(".modal-body #selesai_lakin").val(selesailakin);
						$(".modal-body #lama_lakin").val(lamalakin);
						$(".modal-body #output_lakin").val(outputlakin);
					})


					$(document).ready(function(e){
						$("#form").on("submit", (function(e){
							e.preventDefault();
							$.ajax({
								url : 'models/proses_edit_lakin.php',
								type : 'POST',
								data : new FormData(this),
								contentType : false,
								cache : false,
								processData : false,
								success : function(msg) {
									$('.table').html(msg);
								}
							})
						}))
					})
				</script>

        	</div>
        	
        </div>



<?php 
} else if(@$_GET['act'] == 'del') {
	$lakin->hapus($_GET['id']);
	header("location: ?page=lakin");
}