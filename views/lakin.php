<?php 
ini_set('display_errors', 1);
error_reporting(E_ALL);

include "models/m_lakin.php";


$lakin =  new Lakin("localhost","root","root","lakin");

if(@$_GET['act'] == '') {
?>

		<div class="row">
          <div class="col-lg-12">
            <h1>L-Kin <small>Laporan Kinerja</small></h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="icon-dashboard"></i> L-Kin</a></li>
            </ol>
          </div>
        </div>

        <div class="row">
        	<div class="col-lg-12">
        		
        		<a target="_blank" href="pdf/laporan.php" class="btn btn-primary">Cetak PDF</a><br>
        		<div class="pull-right">

<input type="hidden" name="page" value="lakin">

<form class="form-inline" action="">
	<input type="hidden" name="page" value="lakin">
  <div class="form-group">
    <label class="sr-only" for="exampleInputEmail3">Email address</label>
<input type="date" name="dari" class="form-control" value="<?php echo @$_GET['dari'] ?>">
  </div>
  <div class="form-group">
    <label class="sr-only" for="exampleInputPassword3">Password</label>
<input type="date" name="sampai" class="form-control" value="<?php echo @$_GET['sampai'] ?>">
  </div>


  <div class="form-group">
    <label class="sr-only" for="exampleInputPassword3">Password</label>
<button type="submit" class="btn btn-sm btn-primary">Filter</button>
  </div>




</form>
        	</div>
        	<br>
        	<br>
				<div class="table-responsive">
					
					<?php


$user = $_SESSION['userid'];

$dari = @$_GET['dari'];
$sampai = @$_GET['sampai'];

if (!empty($dari) && !empty($sampai)) {
	# code...

$sqlByMonth = "SELECT *,monthname(tgl_lakin) as bln, (TIME_TO_SEC(selesai_lakin) - TIME_TO_SEC(mulai_lakin))/60 AS `menit` FROM tb_lakin where id_user='$user' and tgl_lakin BETWEEN '$dari' AND '$sampai' group by date(tgl_lakin)";

$tanggal = date('d-M-Y',strtotime($dari))." - ".date('d-M-Y',strtotime($sampai));

$sql = "SELECT *,monthname(tgl_lakin) as bln, (TIME_TO_SEC(selesai_lakin) - TIME_TO_SEC(mulai_lakin))/60 AS `menit` FROM tb_lakin where id_user='$user' and tgl_lakin BETWEEN '$dari' AND '$sampai'";
}
else{
	$sqlByMonth = "SELECT *,monthname(tgl_lakin) as bln, (TIME_TO_SEC(selesai_lakin) - TIME_TO_SEC(mulai_lakin))/60 AS `menit` FROM tb_lakin where id_user='$user' group by date(tgl_lakin)";

$sql = "SELECT *,monthname(tgl_lakin) as bln, (TIME_TO_SEC(selesai_lakin) - TIME_TO_SEC(mulai_lakin))/60 AS `menit` FROM tb_lakin where id_user='$user'";

}


@$_SESSION['sql'] = $sqlByMonth;
@$_SESSION['tgl'] = $tanggal;
$query = mysqli_query($conn,$sql) or die(mysqli_error($conn));
?>

<table class="table table-striped" id="datatables">
<thead>
			
<th>Tgl lakin</th>
				
<th>Uraian lakin</th>
				
<th>Mulai lakin</th>
				
<th>Selesai lakin</th>
				
<th>Output lakin</th>
<th>Status</th>
		<th>Aksi</th>
</thead>
	
<tbody>
<?php while($d=mysqli_fetch_object($query)): ?>

<tr>
						

	<td><?php  echo $d->tgl_lakin; ?></td>
								

	<td><?php  echo $d->uraian_lakin; ?></td>
								

	<td><?php  echo $d->mulai_lakin; ?></td>
								

	<td><?php  echo $d->selesai_lakin; ?></td>
								

	<td><?php  echo $d->output_lakin.' '.$d->satuan; ?></td>
	<td>
		<?php if ($d->status=='1'): ?>
			Diterima
		<?php else: ?>
				<?php if ($d->status=='2'): ?>
					Ditolak
				<?php else: ?>
						<?php if ($d->status=='3'): ?>
							Dibatalkan
						<?php endif ?>
				<?php endif ?>
			<?php endif ?>

	</td>
						<td>
		<a class="btn btn-success" href="?page=lakin&act=edit&act=edit&id=<?php echo $d->id_lakin; ?>">Ubah</a>
	<a class="btn btn-danger" href="?page=lakin&act=del&id=<?php echo $d->id_lakin; ?>">Hapus</a>

	</td>
</tr>
<?php endwhile ?>
</tbody>
</table>




				</div>
<!-- 
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#tambah">Tambah Data</button> -->
				<a href="index.php?page=lakin&act=tambah" class="btn btn-success">Tambah Data</a>

				<div id="tambah" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Tambah Laporan</h4>
							</div>
							<form action="" method="post" enctype="multipart/form-data">
								<div class="modal-body">
									<div class="form-group">
										<label class="control-label" for="tgl_lakin">Tanggal</label>
										<input type="date" name="tgl_lakin" class="form-control" id="tgl_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="uraian_lakin">Kegiatan</label>
										<input type="text" name="uraian_lakin" class="form-control" id="uraian_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="mulai_lakin">Mulai Pengerjaan</label>
										<input type="time" name="mulai_lakin" class="form-control" id="mulai_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="selesai_lakin">Selesai Pengerjaan</label>
										<input type="time" name="selesai_lakin" class="form-control" id="selesai_lakin" required>
									</div>
<!-- 									<div class="form-group">
										<label class="control-label" for="lama_lakin">Lama Pengerjaan (Menit)</label>
										<input type="text" name="lama_lakin" class="form-control" id="lama_lakin" required>
									</div> -->
									<div class="form-group">
										<label class="control-label" for="output_lakin">Kuantitas / Output</label>
										<input type="text" name="output_lakin" class="form-control" id="output_lakin" required>
									</div>
								</div>
								<div class="modal-footer">
									<button type="reset" class="btn btn-danger">Reset</button>
									<input type="submit" class="btn btn-success" name="tambah" value="Simpan">
								</div>
							</form>
							<?php 
							if (@$_POST['tambah']) {
								$tgl_lakin = $connection->conn->real_escape_string($_POST['tgl_lakin']);
								$uraian_lakin = $connection->conn->real_escape_string($_POST['uraian_lakin']);
								$mulai_lakin = $connection->conn->real_escape_string($_POST['mulai_lakin']);
								$selesai_lakin = $connection->conn->real_escape_string($_POST['selesai_lakin']);
								// $lama_lakin = $connection->conn->real_escape_string($_POST['lama_lakin']);
								$output_lakin = $connection->conn->real_escape_string($_POST['output_lakin']);
												
								$lakin->tambah($tgl_lakin, $uraian_lakin, $mulai_lakin, $selesai_lakin, 0, $output_lakin);
								header("location: ?page=lakin");
								
							}
							?>
						</div>
					</div>
				</div>



				<div id="edit" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Edit Laporan</h4>
							</div>
							<form id="form" enctype="multipart/form-data">
								<div class="modal-body" id="modal-edit">
									<div class="form-group">
										<label class="control-label" for="tgl_lakin">Tanggal</label>
										<input type="hidden" name="id_lakin" id="id_lakin">
										<input type="date" name="tgl_lakin" class="form-control" id="tgl_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="uraian_lakin">Kegiatan</label>
										<input type="text" name="uraian_lakin" class="form-control" id="uraian_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="mulai_lakin">Mulai Pengerjaan</label>
										<input type="time" name="mulai_lakin" class="form-control" id="mulai_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="selesai_lakin">Selesai Pengerjaan</label>
										<input type="time" name="selesai_lakin" class="form-control" id="selesai_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="lama_lakin">Lama Pengerjaan (Menit)</label>
										<input type="text" name="lama_lakin" class="form-control" id="lama_lakin" required>
									</div>
									<div class="form-group">
										<label class="control-label" for="output_lakin">Kuantitas / Output</label>
										<input type="text" name="output_lakin" class="form-control" id="output_lakin" required>
									</div>
								</div>
								<div class="modal-footer">
									<input type="submit" class="btn btn-success" name="edit" value="Simpan">
								</div>
							</form>
						</div>
					</div>
				</div>


				<script src="assets/js/jquery-1.10.2.js"></script>
				<script type="text/javascript">
					$(document).on("click", "#edit_lakin", function(){
						var idlakin = $(this).data('id');
						var tgllakin = $(this).data('tgl');
						var uraianlakin = $(this).data('uraian');
						var mulailakin = $(this).data('mulai');
						var selesailakin = $(this).data('selesai');
						var lamalakin = $(this).data('lama');
						var outputlakin = $(this).data('output');
						$(".modal-body #id_lakin").val(idlakin);
						$(".modal-body #tgl_lakin").val(tgllakin);
						$(".modal-body #uraian_lakin").val(uraianlakin);
						$(".modal-body #mulai_lakin").val(mulailakin);
						$(".modal-body #selesai_lakin").val(selesailakin);
						$(".modal-body #lama_lakin").val(lamalakin);
						$(".modal-body #output_lakin").val(outputlakin);
					})


					$(document).ready(function(e){
						$("#form").on("submit", (function(e){
							e.preventDefault();
							$.ajax({
								url : 'models/proses_edit_lakin.php',
								type : 'POST',
								data : new FormData(this),
								contentType : false,
								cache : false,
								processData : false,
								success : function(msg) {
									$('.table').html(msg);
								}
							})
						}))
					})
				</script>

        	</div>
        	
        </div>



<?php 
} elseif(@$_GET['act'] == 'del') {
	// $lakin->hapus($_GET['id']);
	// header("location: ?page=lakin");

	$id = $_GET['id'];

	$sql = "DELETE FROM tb_lakin WHERE id_lakin='$id'";
	$query = mysqli_query($conn,$sql) or die(mysqli_error($conn));
	if ($query) {
		# code...
		header("location: ".$_SERVER['HTTP_REFERER']);
	}
}

elseif (@$_GET['act']=='edit') {
	# code...e
	$id = @$_GET['id'];

	include('lakin/edit.php');
// echo $id;
}
elseif (@$_GET['act']=='tambah') {

	include('lakin/tambah.php');

}

?>

