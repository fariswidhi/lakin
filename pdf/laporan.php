<?php 
session_start();

ini_set('display_errors', 1);
error_reporting(E_ALL);

include "../models/m_lakin.php";
include "../config/conn.php";

if (empty($_SESSION['userid'])) {
  header("location: ../login.php");
}

$userid = @$_SESSION['userid'];
$sql_user = "SELECT * FROM tb_user WHERE id_user='$userid'";
$query_user = mysqli_query($conn,$sql_user);
$data = mysqli_fetch_array($query_user);

$nama_lengkap = $data['nama_lengkap'];

$lakin =  new Lakin("localhost","root","root","lakin");



            $no = 1;
            // $tampil = $lakin->tampil();
            $user = @$_GET['user'];
            // if ($data['level']=='2') {
            //   # code...

            // $sql = "SELECT *, (TIME_TO_SEC(selesai_lakin) - TIME_TO_SEC(mulai_lakin))/60 AS `menit` FROM tb_lakin WHERE id_user='$userid' ORDER BY tgl_lakin DESC ";

            // }
            // else{

            //   if ($user==null) {

            // $sql = "SELECT *, (TIME_TO_SEC(selesai_lakin) - TIME_TO_SEC(mulai_lakin))/60 AS `menit` FROM tb_lakin ORDER BY tgl_lakin DESC "; 


            // }
            // else{
            // $sql = "SELECT *, (TIME_TO_SEC(selesai_lakin) - TIME_TO_SEC(mulai_lakin))/60 AS `menit` FROM tb_lakin WHERE id_user='$user' ORDER BY tgl_lakin DESC ";

            // }
            // }

            $sql = $_SESSION['sql'];

            $query  = mysqli_query($conn,$sql) or die(mysqli_error($conn));


$tgl = @$_SESSION['tgl'];



 ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>

    <link href="../assets/css/bootstrap.css" rel="stylesheet">

</head>
<body style="padding: 5px;"  onload="window.print()">

	<div style="width: 100%;margin: 0 auto;float: none;">
		<div style="width: 12%;float: left;">
			<img src="../assets/img/kalimantan_utara.png" style="width: 100%;height: 100%;">
		</div>
		<div style="width: 70%;float: left;">
			<center><h4 style="text-transform: uppercase;padding: 0;padding-top:10px;margin: 0;">Pemerintah Provinsi Kalimantan Utara</h4>
			<h4 style="text-transform: uppercase;padding: 0;margin: 0;">DINAS PENDIDIKAN DAN KEBUDAYAAN</h4>

				<h4 style="text-transform: uppercase;font-weight: bold;padding: 0;margin: 0;">Smk Negeri 1 Nunukan</h5>
				<h4 style="padding: 0;margin: 0;">Terakreditasi “A”</h4>
				<span>Jl. Sei. Fatimah, Kec. Nunukan, Kab. Nunukan 77482, Prov. Kalimantan Utara</span><br>
				<span>Telp/Fax : 0556-2027227/2027228, Email : smkn1nunukan@gmail.com</span><br>
				<b><span>NSS: 301160804011   NPSN: 30402818</span></b>
			</center>

		</div>
		<div style="width: 12%;float: left;">
			<img src="../assets/img/smkn_nunukan.png" style="width: 100%;height: 100%;">
		</div>


		<div style="width: 100%;float: left;">
<hr>
<h3>Laporan Harian</h3>
<?php if (!empty($tgl)): ?>
  <h3><?php echo "Data Tanggal ".$tgl ?></h3>
<?php endif ?>

<h4></h4>
<table class="table table-bordered" style="margin-top: 30px;float: left;font-size: 10px;">

  <col>
  <colgroup span="2"></colgroup>
  <colgroup span="2"></colgroup>
  <tr>

    <th rowspan="2"   style="vertical-align : middle;text-align:center;"><center>NO</center></th>
  	<th rowspan="2"   style="vertical-align : middle;text-align:center;"  ><center>Tanggal</center></th>

    <th rowspan="2"   style="vertical-align : middle;text-align:center;"  ><center>Kegiatan</center></th>
    <th colspan="3" scope="colgroup"><center>Waktu Pengerjaan</center></th>

  	<th rowspan="2"  style="vertical-align : middle;text-align:center;"><center>Kuantitas/Output</center></th>
  	<th colspan="3"  ><center>Verifikasi</center></th>
  </tr>
  <tr>
    <th scope="col">Mulai Pengerjaan</th>
    <th scope="col">Selesai Pengerjaan</th>
    <th scope="col">Lama Pengerjaan(menit)</th>
    <th scope="col">TR</th>
    <th scope="col">TL</th>
    <th scope="col">BT</th>
  </tr>

<?php if (mysqli_num_rows($query)==0): ?>
<tr>
  <td colspan="9"><center>Tidak Ada Data</center></td>
</tr>  

  <?php else: ?>
                <?php 

            $no = 1;
            // $tampil = $lakin->tampil();

            $byUser = @$_SESSION['byUser'];
            while($data = mysqli_fetch_object($query)) {
              $total  = 0;


              $sqlByDate = "SELECT *,(TIME_TO_SEC(selesai_lakin) - TIME_TO_SEC(mulai_lakin))/60 AS `menit` from tb_lakin where tgl_lakin='$data->tgl_lakin' ";

              if ($byUser!=null) {
                # code...
                $sqlByDate .= " AND ".$byUser;
              }





              // echo $sqlByDate;
              $query1 = mysqli_query($conn,$sqlByDate) or die(mysqli_error($conn));


            ?>



  <tr>
    <th scope="row"><?php echo $no++ ?></th>
    <td><?php echo date('d-m-Y',strtotime($data->tgl_lakin)) ?></td>



    <?php 
    $n=0;
$jam = 0;
  $rows = 0;
      while ($d=mysqli_fetch_object($query1)) {
$jam += $d->menit*60;
  $rows += 1;
?>

  <?php if ($n++>0): ?>
    <th></th>
    <th></th>
  <?php endif ?>

    <th><?php echo $d->uraian_lakin ?></th>
    <td><?php echo $d->mulai_lakin ?></td>
    <td><?php echo $d->selesai_lakin ?></td>
    <td><?php echo (int) $d->menit ?></td>
    <td><?php echo $d->output_lakin.' '.$d->satuan ?></td>
    <td><center><?php echo $d->status=="1" ? "V":"" ?></center></td>
    <td><center><?php echo $d->status=="2" ? "V":"" ?></center></td>

    <td><center><?php echo $d->status=="3" ? "V":"" ?></center></td>
  </tr>
<?php
      }



     ?>
<tr>
  
  <td colspan="3"><center><b>Total Jam Kerja Efektif</b></center></td>
  <td colspan="3"><center><b><?php echo gmdate("H",$jam)." Jam ".gmdate("i",$jam)." Menit " ?></b></center></td>
  <td colspan="4"></td>
</tr>

 <?php } 

 ?>
</tr>

<?php endif ?>
</table>

<br>

<br>

<?php 
$sql = "SELECT * FROM tb_user where level ='1' limit 0,1";
$query = mysqli_query($conn,$sql);
$data = mysqli_fetch_array($query);
$nama_kepsek = mysqli_num_rows($query) > 0 ? $data['nama_lengkap'] : '';

 ?>


<div style="width: 100%;float: left;">
  <div style="width: 50%;float: left;">
    <center>Pejabat Penilai,
    <br><br>
    <br>
    <br>
    <br>

    <span style="font-size: 15px;"><?php echo $nama_kepsek ?></span>
    <br><hr style="width: 50%;border-bottom: 1px solid #333;margin: 0;padding: 0;">    NIP. <?php echo $data['nip'] ?>

    </center>

  </div>
  <div style="width: 50%;float: left;">

    <center>Tanjung Selor, <?php echo date('d-m-Y'); ?> <br>
      Pegawai Yang Membuat



    <span style="font-size: 15px;">
      <br>
      <br>
      <br>
      <br>
      <?php echo $nama_lengkap ?>
    </span>
    <br><hr style="width: 50%;border-bottom: 1px solid #333;margin: 0;padding: 0;">    
    </center>

  </div>
</div>

		</div>
	</div>


  <style type="text/css">

  @media print {

  html, body {
    height:100%; 
    margin: 0 !important; 
    padding: 0 !important;
    overflow: hidden;
  }

}



    table.table-bordered{
    border:1px solid blue;
    margin-top:20px;
  }
table.table-bordered > thead > tr > th{
    border:1px solid blue;
}

.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{
  border: 1px solid #000;
}


table.table-bordered > tbody > tr > td{
    border:1px solid #000;

  }

  @media print{
        .table thead tr td,.table tbody tr td{
            border-width: 1px !important;
            border-style: solid !important;
            border-color: black !important;
            font-size: 10px !important;
            background-color: red;
            padding:0px;
            -webkit-print-color-adjust:exact ;
        }


        .table thead tr td,.table tbody tr th{
            border-width: 1px !important;
            border-style: solid !important;
            border-color: black !important;
            font-size: 10px !important;

            -webkit-print-color-adjust:exact ;
        }
    }


  </style>
</body>
</html>