<?php 
class Lakin {

	private $mysqli;

	function __construct($host,$user,$pass,$db) {
		$this->mysqli = mysqli_connect($host,$user,$pass,$db);
	}

	public function tampil($id = null) {
		$db = $this->mysqli;
		$sql = "SELECT *, (TIME_TO_SEC(selesai_lakin) - TIME_TO_SEC(mulai_lakin))/60 AS `menit` FROM tb_lakin ORDER BY tgl_lakin DESC ";
		if($id != null) {
			$sql .= " WHERE id_lakin = $id";
		}
		$query = $db->query($sql) or die ($db->error);
		return $query;
	}

	public function tambah($tgl_lakin, $uraian_lakin, $mulai_lakin, $selesai_lakin, $lama_lakin, $output_lakin) {
		$db = $this->mysqli->conn;
		$db->query("INSERT INTO tb_lakin VALUES ('', '$tgl_lakin', '$uraian_lakin', '$mulai_lakin', '$selesai_lakin', '$lama_lakin', '$output_lakin')") or die ($db->error);
	}

	public function edit($sql) {
		$db = $this->mysqli->conn;
		$db->query($sql) or die ($db->error);
	}

	public function hapus($id) {
		$db = $this->mysqli->conn;
		$db->query("DELETE FROM tb_lakin WHERE id_lakin = '$id'") or die ($db->error);
	}

	function __destruct() {
		$db = $this->mysqli;
		$db->close();
	}
}
?>