-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 04, 2018 at 04:30 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 5.6.37-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lakin`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_lakin`
--

CREATE TABLE `tb_lakin` (
  `id_lakin` int(6) NOT NULL,
  `tgl_lakin` date NOT NULL,
  `uraian_lakin` varchar(255) NOT NULL,
  `mulai_lakin` time NOT NULL,
  `selesai_lakin` time NOT NULL,
  `lama_lakin` varchar(50) DEFAULT NULL,
  `output_lakin` varchar(100) NOT NULL,
  `satuan` varchar(10) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;









--
-- Dumping data for table `tb_lakin`
--

INSERT INTO `tb_lakin` (`id_lakin`, `tgl_lakin`, `uraian_lakin`, `mulai_lakin`, `selesai_lakin`, `lama_lakin`, `output_lakin`, `satuan`, `id_user`, `status`) VALUES
(1, '2018-09-28', 'Upacara pelepasan Siswa PKL', '09:00:00', '10:30:00', '150 Menit', '20', NULL, 1, 1),
(2, '2018-09-28', 'Pengarahan siswa PKL Kelas XII Multimedia', '11:00:00', '12:00:00', '60 Menit', '1 Kegiatan', NULL, 1, 1),
(6, '2012-12-12', 'ok', '12:12:00', '12:12:00', NULL, 'ok', 'Kegiatan', NULL, NULL),
(7, '2012-12-12', 'ok', '12:12:00', '14:12:00', NULL, 'ok', 'Kegiatan', 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` text,
  `level` int(11) DEFAULT NULL,
  `nama_lengkap` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `level`, `nama_lengkap`) VALUES
(1, 'guru', '77e69c137812518e359196bb2f5e9bb9', 2, ''),
(2, 'kepsek', '8561863b55faf85b9ad67c52b3b851ac', 1, 'SUHARSO'),
(3, 'bobo', 'ca2cd2bcc63c4d7c8725577442073dde', 2, 'BOBO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_lakin`
--
ALTER TABLE `tb_lakin`
  ADD PRIMARY KEY (`id_lakin`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_lakin`
--
ALTER TABLE `tb_lakin`
  MODIFY `id_lakin` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
