<?php 
ob_start();
session_start();
require_once('config/+koneksi.php');
require_once('models/database.php');
require('config/conn.php');


if (empty($_SESSION['userid'])) {
  # code...
  header("location: login.php");  
}

$userid = $_SESSION['userid'];
$sql = "SELECT * FROM tb_user where id_user ='$userid'";
$query = mysqli_query($conn,$sql);
$data = mysqli_fetch_array($query);

$level = $data['level'];

$connection = new Database($host, $user, $pass, $database);

// echo "string";

// print_r($data);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   
    <title>Laporan Kinerja</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/dataTables/datatables.min.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="assets/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="">Laporan Kinerja</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
            <li><a href="?page=dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="?page=user"><i class="fa fa-users"></i> User</a></li>
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> L-Kin <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <?php if ($level=="2"): ?>
                  
                <li><a href="?page=lakin">Data Kinerja</a></li>

                <?php endif ?>
                <?php if ($level=="1"): ?>
                  <li><a href="?page=lakin-guru">Data Kinerja Guru</a></li>
                <?php endif ?>
                <li><a href="#">Grafik</a></li>
                <li><a href="#">Report</a></li>
              </ul>
            </li>
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $data['username'] ?> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                <li><a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge">7</span></a></li>
                <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                <li class="divider"></li>
                <li><a href="logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>

      <div id="page-wrapper">

        <?php

        if(@$_GET['page'] == 'dashboard' || @$_GET['page'] == '') {
        include "views/dashboard.php";
      } else if (@$_GET['page'] == 'lakin') {
        include "views/lakin.php";
      } 
else if (@$_GET['page'] == 'lakin-guru') {
        include "views/lakin-guru.php";
      } 

else if (@$_GET['page'] == 'lakin-guru') {
        include "views/lakin-guru.php";
      } 
else if (@$_GET['page'] == 'user') {
        include "views/user.php";
      } 

        ?>

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/dataTables/datatables.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript">
      $(".select2").select2();
      $(document).ready(function() {
        $('#datatables').DataTable();
      });
    </script>
  </body>
</html>